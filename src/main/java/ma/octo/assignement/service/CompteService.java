package ma.octo.assignement.service;


import ma.octo.assignement.models.Compte;
import ma.octo.assignement.repository.CompteRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
@Service
public class CompteService {
    private CompteRepository compteRepository;

    public List<Compte> getComptes() {
        List<Compte> all = compteRepository.findAll();
        return CollectionUtils.isEmpty(all) ? null : all;
    }
    // We added this methods because we need them in transfert Service
    public Compte getCompteByNb(String nrCompte) {
        return compteRepository.findByNrCompte(nrCompte);
    }


    public Compte updateCompte(Compte compte) {
        return compteRepository.save(compte);
    }
    public void saveCompte(Compte compte) {
        compteRepository.save(compte);
    }


}
