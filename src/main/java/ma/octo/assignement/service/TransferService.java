package ma.octo.assignement.service;

import lombok.AllArgsConstructor;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.models.Compte;
import ma.octo.assignement.models.Transfer;
import ma.octo.assignement.repository.TransferRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

@Service
@AllArgsConstructor
public class TransferService {
    public static final int MONTANT_MAX = 10000;
    private TransferRepository transferRepository;
    private AuditService auditService;
    private CompteService compteService;
    private TransferMapper transferMapper;

    public List<Transfer> getTransfers() {
        List<Transfer> all = transferRepository.findAll();
        // if the collection is empty we should return null, else we should return all
        return CollectionUtils.isEmpty(all) ? null : all;
    }

    public Transfer createTransfer(TransferDto transferDto)
            throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {

        Compte Emetteur = compteService.getCompteByNb(transferDto.getNrCompteEmetteur());
        Compte Destination = compteService.getCompteByNb(transferDto.getNrCompteBeneficiaire());

        if (Emetteur == null || Destination== null) {
            System.out.println("Le compte n'existe pas");
            throw new CompteNonExistantException("Le compte n'existe pas");
        }

        if (transferDto.getMontant() == null || transferDto.getMontant().equals(BigDecimal.ZERO)) {
            System.out.println("Le montant est vide");
            throw new TransactionException("Le montant est vide");
        }
        else if (transferDto.getMontant().intValue() < 10) {
            System.out.println("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de transfer non atteint");
        }


        else if (transferDto.getMontant().intValue() > MONTANT_MAX) {
            System.out.println("Le montant maximal est dépassé");
            throw new TransactionException("Le montant maximal estdépassé");
        }
        if (transferDto.getMotif().length() <= 0) {
            System.out.println("Le motif est vide");
            throw new TransactionException("Le motif est vide");
        }
        if (Emetteur.getSolde().subtract(transferDto.getMontant()).compareTo(BigDecimal.ZERO) < 0) {
            System.out.println("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
        }


        Emetteur.setSolde(Emetteur.getSolde().subtract(transferDto.getMontant()));
        compteService.updateCompte(Emetteur);
        Destination.setSolde(Destination.getSolde().add(transferDto.getMontant()));
        compteService.updateCompte(Destination);

        Transfer transfer = transferMapper.transferDtoToTransfer(transferDto);
        transfer.setCompteBeneficiaire(Destination);
        transfer.setCompteEmetteur(Emetteur);
        transfer = transferRepository.save(transfer);

        auditService.auditTransfer("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant().toString());
        return transfer;
    }
}

