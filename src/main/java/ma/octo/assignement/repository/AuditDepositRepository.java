package ma.octo.assignement.repository;

import ma.octo.assignement.models.AuditDeposit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuditDepositRepository extends JpaRepository<AuditDeposit, Long> {
}
