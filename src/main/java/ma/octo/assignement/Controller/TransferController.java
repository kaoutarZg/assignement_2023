package ma.octo.assignement.Controller;

import lombok.AllArgsConstructor;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.models.Compte;
import ma.octo.assignement.models.Transfer;
import ma.octo.assignement.models.Utilisateur;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.TransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
//The path should be specified in the @RequestMapping annotation, not in @RestController
@RequestMapping(value = "/transfers")
@AllArgsConstructor
class TransferController {

    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(TransferController.class);

    private TransferService transferService;

    @GetMapping("")
    public ResponseEntity<List<Transfer>> loadAllTransfert() {

        return new ResponseEntity<>(
                transferService.getTransfers(),
                HttpStatus.OK
        );

    }

    @PostMapping("")
    public ResponseEntity<Void> createTransfer(@RequestBody TransferDto transferDto) throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException {
        Transfer transfer = transferService.createTransfer(transferDto);
        return new ResponseEntity<>(
                HttpStatus.CREATED
        );
    }

}
