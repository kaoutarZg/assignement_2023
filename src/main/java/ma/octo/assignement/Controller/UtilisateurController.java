package ma.octo.assignement.Controller;


import lombok.AllArgsConstructor;
import ma.octo.assignement.models.Utilisateur;
import ma.octo.assignement.service.UtilisateurService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/users")

public class UtilisateurController {

    private UtilisateurService utilisateurService;
    @GetMapping("")
    public ResponseEntity<List<Utilisateur>> loadAllUtilisateur() {

        return new ResponseEntity<>(
                utilisateurService.getUtilisateurs(),
                HttpStatus.OK
        );
    }


}
