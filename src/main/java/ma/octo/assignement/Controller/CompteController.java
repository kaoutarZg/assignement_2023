package ma.octo.assignement.Controller;


import lombok.AllArgsConstructor;
import ma.octo.assignement.models.Compte;
import ma.octo.assignement.service.CompteService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/accounts")

public class CompteController {
    private CompteService compteService;

    @GetMapping("")
    public ResponseEntity<List<Compte>> loadAllCompte() {

        return new ResponseEntity<>(
                compteService.getComptes(),
                HttpStatus.OK
        );
    }


}
