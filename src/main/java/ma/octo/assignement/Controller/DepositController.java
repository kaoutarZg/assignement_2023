package ma.octo.assignement.Controller;

import lombok.AllArgsConstructor;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.models.Deposit;
import ma.octo.assignement.service.DepositService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/deposits")
@AllArgsConstructor
class DepositController {

    private DepositService depositService;

    @GetMapping("")
    public ResponseEntity<List<Deposit>> loadAllDepositss() {
        return new ResponseEntity<>(
                depositService.getDeposits(),
                HttpStatus.OK
        );
    }


    @PostMapping("")
    public ResponseEntity<Void> createDeposit(
            @RequestBody DepositDto depositDto
    ) throws TransactionException, CompteNonExistantException {
        Deposit deposit =depositService.createDeposit(depositDto);
        return new ResponseEntity<>(
                HttpStatus.CREATED
        );
    }

}

