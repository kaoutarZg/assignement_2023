package ma.octo.assignement.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "DEP")
//Implementation of deposit models
public class Deposit extends Operation{
  @Column(length = 50, nullable = false)
  private String nameDepositor;
}
