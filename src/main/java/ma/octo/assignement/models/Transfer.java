package ma.octo.assignement.models;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "TRAN")
@Getter
@Setter
public class Transfer extends Operation{
  @ManyToOne
  private Compte compteEmetteur;

}
